import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class kadai {
    private JPanel root;
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button6;
    private JButton checkOutButton;
    private JTextPane Total;
    private JTextPane List;
    int sum = 0;


    public void order(String food,String money,int price){
        int confirmation = JOptionPane.showConfirmDialog(null,
              "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if (confirmation == 0){
            String currentText = List.getText();
            List.setText(currentText + food + " " + money +"\n");
            JOptionPane.showMessageDialog(null,
                    "Thank you for ordering " + food + "! It will be served as soon as possible."
                    );
            List.getText();
            sum = sum + price;
            Total.setText("Total " + sum + " yen");
        }
    }

    public kadai() {
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("tare_negima","150yen",150);
            }
        });
        button1.setIcon(new ImageIcon(
                this.getClass().getResource("kizokuyaki_momo_tare.jpg")
        ));

        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("piman","200yen",200);
            }
        });
        button2.setIcon(new ImageIcon(
                this.getClass().getResource("piman.jpg")
                ));

        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("shio_kawa","300yen",300);
            }
        });
                button3.setIcon(new ImageIcon(
                        this.getClass().getResource("shio_kawa.jpg")
                ));
        button4.addActionListener(new ActionListener() {
                                      @Override
                                      public void actionPerformed(ActionEvent e) {
                                          order("shio_sunazuri","350yen",350);
                                      }
                                  });
                button4.setIcon(new ImageIcon(
                        this.getClass().getResource("shio_sunazuri.jpg")
                ));
        button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("shio_tsukune","500yen",500);
            }
        });
        button5.setIcon(new ImageIcon(
                this.getClass().getResource("shio_tsukune.jpg")
        ));
        button6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("tare_heart","1000yen",1000);
            }
        });
        button6.setIcon(new ImageIcon(
                this.getClass().getResource("tare_heart.jpg")
        ));


        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int checkout = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if (checkout == 0) {
                    JOptionPane.showConfirmDialog(null,
                            "Thank you. The total price is "+sum +" yen.",
                            "Massage",
                            JOptionPane.CLOSED_OPTION);
                    List.setText("");
                    Total.setText("Total 0 yen");
                }
            }
        });
    }



    public static void main(String[] args) {
        JFrame frame = new JFrame("kadai");
        frame.setContentPane(new kadai().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
